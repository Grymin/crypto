#include "CryptoAnalyzer.hpp"
#include "BasicHelper.hpp"
#include "SystemMock.hpp"

#include <gtest/gtest.h>
#include <gmock/gmock.h>

namespace {

struct CryptoAnalyzerTest : ::testing::Test
{
    using SUT = CryptoAnalyzer;
    SystemMock system;
    SUT sut;
};

TEST_F(CryptoAnalyzerTest, ShouldCallScript)
{
    EXPECT_CALL(system, system(::testing::StrEq("date"))).WillOnce(::testing::Return(EXIT_SUCCESS));
    sut.analyze();
}

}
