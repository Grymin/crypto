#include "SystemMock.hpp"

SystemMock::SystemMock()
{
    callSystem = [this](auto cmd) { return this->system(cmd); };
}

std::function<SystemMock::system_t> SystemMock::callSystem;

int system(const char* cmd)
{
    return SystemMock::callSystem(cmd);
}

