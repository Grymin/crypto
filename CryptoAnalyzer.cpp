#include "CryptoAnalyzer.hpp"
#include "helpers/BasicHelper.hpp"
#include "helpers/advanced/AdvHelper.hpp"

#include <predi/Predi.hpp>
#include <cstdlib>

float CryptoAnalyzer::complexity() const
{
    return predi::predict({}, {});
}

void CryptoAnalyzer::analyze() const
{
    system("date");
}
